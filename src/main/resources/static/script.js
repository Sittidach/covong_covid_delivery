var register_title          = document.getElementById("register-title");
var register_input_email    = document.getElementById("register-input-email");
var register_message_status = document.getElementById("register-message-status");
var register_button_submit  = document.getElementById("register-button-submit");
var register_link_other     = document.getElementById("register-link-other");

register_button_submit.onclick = register_post_send;

register_input_email.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
        register_button_submit.click();
    }
    console.log(event.key);
});

function register_post_send() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState != 4) {
            return;
        }

        if (this.status == 200) {
            register_message_status.classList.add("register-message-status-success");
            register_message_status.innerHTML = "อีเมลถูกส่งเพื่อยืนยันตัวตน";
            register_input_email.setAttribute("placeholder", register_input_email.value);
            register_input_email.value = "";
            return;
        }

        if (this.status == 417) {
            register_message_status.classList.add("register-message-status-fail");
            let responeJson = JSON.parse(this.response);
            register_error_handler(responeJson.error);
        }
        console.log(this.status);

    };
    register_message_status.classList.remove("register-message-status-fail", "register-message-status-success");
    register_message_status.innerHTML = "กำลังส่ง..."
    xhttp.open("POST", "http://13.229.230.53/register", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
        email: register_input_email.value
    }));
}

function register_error_handler(error_message) {
    switch (error_message) {
        case "user.register.email.null":
            register_message_status.innerHTML = "กรุณากรอกอีเมล";
            break;
        case "user.register.email.duplicated":
            register_message_status.innerHTML = "อีเมลซ้ำ";
            break;
        case "user.register.email.invaild":
            register_message_status.innerHTML = "อีเมลไม่ถูกต้อง";
            break;
    }
}

function unregister_post_send() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState != 4) {
            return;
        }

        if (this.status == 200) {
            register_message_status.innerHTML = "อีเมลถูกส่งเพื่อยืนยันตัวตน";
            register_input_email.setAttribute("placeholder", register_input_email.value);
            register_input_email.value = "";
            return;
        }

        if (this.status == 417) {
            register_message_status.classList.add("register-message-status-fail");
            let responeJson = JSON.parse(this.response);
            unregister_error_handler(responeJson.error);
        }
        console.log(this.status);

    };
    register_message_status.classList.remove("register-message-status-fail");
    register_message_status.innerHTML = "กำลังส่ง..."
    xhttp.open("PUT", "http://13.229.230.53/register", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
        email: register_input_email.value
    }));
}

function unregister_error_handler(error_message) {
    switch (error_message) {
        case "user.unregister.email.null":
            register_message_status.innerHTML = "กรุณากรอกอีเมล";
            break;
        case "user.unregister.email.not_found":
            register_message_status.innerHTML = "ไม่พบอีเมลนี้";
            break;
        case "user.unregister.email.not_confirm":
            register_message_status.innerHTML = "ยังไม่ได้ยืนยันตัวตน";
            break;
    }
}

function register_mode_toggle() {
    if (register_link_other.innerHTML == "ยกเลิกรับข้อมูล") {
        register_title.innerHTML = "ยกเลิกรับข้อมูลทางอีเมล";
        register_button_submit.innerHTML = "ตกลง";
        register_button_submit.onclick = unregister_post_send;
        register_link_other.innerHTML = "สมัครรับข้อมูล";
        return;
    }
    register_title.innerHTML = "สมัครรับข้อมูลทางอีเมล";
    register_button_submit.innerHTML = "สมัคร";
    register_button_submit.onclick = register_post_send;
    register_link_other.innerHTML = "ยกเลิกรับข้อมูล";
}

function hide_text(self) {
    self.innerHTML = "";
}

// Javascript basic => www.w3school.com
// DOM | BOM => www.w3school.com