package tsu.cs.advanced_web.covong_covid_delivery.model;

import lombok.Data;

@Data
public class UserRegisterModel {
    private String email;
}
