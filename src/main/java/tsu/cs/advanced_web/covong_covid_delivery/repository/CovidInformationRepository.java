package tsu.cs.advanced_web.covong_covid_delivery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import tsu.cs.advanced_web.covong_covid_delivery.model.CovidInformationModel;

public interface CovidInformationRepository extends CrudRepository<CovidInformationModel, Integer>{

    Optional<CovidInformationModel> findByTxnDate(String txnDate);

    @Query(
        value = "SELECT * FROM covid_information ORDER BY txn_date DESC LIMIT 1",
        nativeQuery = true
    )
    public CovidInformationModel findByLatestTxnDate();
}