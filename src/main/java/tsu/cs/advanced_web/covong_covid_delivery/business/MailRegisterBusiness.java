package tsu.cs.advanced_web.covong_covid_delivery.business;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;

@Service
public class MailRegisterBusiness {

    @Autowired
    private JavaMailSender javaMailSender;

    private TemplateEngine templateEngine;

    @Autowired
    public MailRegisterBusiness(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public void prepareAndSend(String email, String html_page) throws UserException, MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        try {
            helper.setTo(email);
            helper.setSubject("Sent from Covong Covid Delivery");
            helper.setText(html_page, true);
            javaMailSender.send(msg);
        } catch (Exception e) {
            throw UserException.registerEmailInvalid();
        }
    }

    public String registerBuildContent(String verifyCode) {
        Context context = new Context();
        context.setVariable("verify_code", verifyCode);
        return templateEngine.process("mail/mailRegisterTemplate", context);
    }

    public String unregisterBuildContent(String verifyCode) {
        Context context = new Context();
        context.setVariable("verify_code", verifyCode);
        return templateEngine.process("mail/mailUnregisterTemplate", context);
    }
}
