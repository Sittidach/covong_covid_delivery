package tsu.cs.advanced_web.covong_covid_delivery.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tsu.cs.advanced_web.covong_covid_delivery.business.RegisterBusiness;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.UserRegisterModel;

@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private RegisterBusiness registerBusiness;

    @PostMapping
    public ResponseEntity<UserRegisterModel> register(@RequestBody UserRegisterModel email) throws UserException, MessagingException {
        UserRegisterModel respone = registerBusiness.register(email);
        return ResponseEntity.ok(respone);
    }

    @PutMapping
    public ResponseEntity<UserRegisterModel> unregister(@RequestBody UserRegisterModel email) throws UserException, MessagingException {
        UserRegisterModel respone = registerBusiness.unregister(email);
        return ResponseEntity.ok(respone);
    }
}
