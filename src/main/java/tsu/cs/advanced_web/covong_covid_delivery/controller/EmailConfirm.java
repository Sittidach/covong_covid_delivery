package tsu.cs.advanced_web.covong_covid_delivery.controller;

import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import tsu.cs.advanced_web.covong_covid_delivery.business.MailCovidInformationUpdateBusiness;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.UserModel;
import tsu.cs.advanced_web.covong_covid_delivery.repository.UserRepository;

@Controller
@RequestMapping("/confirm")
public class EmailConfirm {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailCovidInformationUpdateBusiness mailCovidInformation;

    @GetMapping("/register")
    public String confirmRegister(@RequestParam("verify_code") String verify_code) throws UserException, MessagingException {
        Optional<UserModel> user_register = userRepository.findByVerifyCode(verify_code);

        if (user_register.isEmpty()) {
            throw UserException.registerConfirmNotFound();
        }

        UserModel user = user_register.get();
        if (user.isEnabled()) {
            throw UserException.registerConfirmInvalid();
        }

        user.setVerifyCode(null);
        user.setEnabled(true);
        userRepository.save(user);

        mailCovidInformation.prepareAndSend(new String[]{user.getEmail()});
        return "mail/mailConfirmRegister";
    }

    @GetMapping("/unregister")
    public String confirmUnregister(@RequestParam("verify_code") String verify_code) throws UserException {
        Optional<UserModel> user_unregister = userRepository.findByVerifyCode(verify_code);

        if (user_unregister.isEmpty()) {
            throw UserException.unregisterConfirmNotFound();
        }

        UserModel user = user_unregister.get();
        if (!user.isEnabled()) {
            throw UserException.unregisterConfirmInvalid();
        }

        userRepository.delete(user);
        return "mail/mailConfirmUnregister";
    }
}
